﻿using UnityEngine;
using System.Collections;

public class GizmoScript : MonoBehaviour {

    public float m_scale = 0.1f;
    public Color m_color = Color.red;
	// Use this for initialization
	void Start () {
	
	}
	
	// Update is called once per frame
	void Update () {
	
	}

    private void OnDrawGizmosSelected()
    {
		Gizmos.color = m_color;
        Gizmos.DrawSphere(transform.position, m_scale);
	}
}
