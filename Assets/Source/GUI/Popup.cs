﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class Popup : MonoBehaviour {

	public BuildingVisual m_visual;

    public Text m_text;

	public static Popup Create (BuildingVisual visual) {
		Vector2 pos = RectTransformUtility.WorldToScreenPoint (Camera.main, visual.transform.position);
        Popup newPop = Instantiate(Config.Me.m_gui.m_popupScript.m_prefabPopup);
		newPop.m_visual = visual;
        var rt = newPop.transform as RectTransform;
        rt.SetParent(Config.Me.m_gui.m_popupScript.transform);
        rt.anchoredPosition = pos;
        return newPop;
	}

	public void Trigger_Cancel ()
	{
//		GetComponent<Animator> ().SetTrigger ("Close");
		Config.Me.m_gui.Trigger_DestroyStructure ();
	}
	
}
