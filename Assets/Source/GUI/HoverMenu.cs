﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class HoverMenu : MonoBehaviour {

	[SerializeField] private Animator m_anim;

	public void Trigger_ToggleMenu ()
	{
		if (Config.Me.m_gui.m_eHudState != GUIController.E_HudState.locked) {
			if (!m_anim.enabled) {
				m_anim.enabled = true;
			}
			bool isOpen = m_anim.GetBool ("isOpen");
			Trigger_ToggleMenu (!isOpen);
		}
	}

	public void Trigger_ToggleMenu (bool isOpen)
	{
		if (Config.Me.m_gui.m_eHudState != GUIController.E_HudState.locked)
			m_anim.SetBool ("isOpen", isOpen);
	}
		
}
