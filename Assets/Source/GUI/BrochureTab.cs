﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BrochureTab : MonoBehaviour {

	public IBrochureTab m_tabData;
	public Text m_text;
	public Image m_image;
	[Range (0.0f, 1.0f)] public float colourPercent;

	void Awake () {
		if (m_image == null)
			m_image = GetComponent<Image> ();
	}

	public void Trigger_OnClick () {
		if (m_tabData != null)
			m_tabData.OnClickEvent ();
	}

	public Color GetTransitionColour (float percent) {
		var unselected = m_tabData != null ? m_tabData.UnselectedColour : Color.grey;
		var selected = m_tabData != null ? m_tabData.SelectedColour : Color.white;
		return Color.Lerp (unselected, selected, percent);
	}

	void Update () {
		m_image.color = GetTransitionColour (colourPercent);
	}

}
