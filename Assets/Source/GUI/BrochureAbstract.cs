﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

public interface IBrochureTab {
	event BrochureAbstract.OnClickHandler OnClick;
	string CategoryName { get; }
	void OnClickEvent ();
	Animator TabAnim { get; set; }
	Color SelectedColour { get; set; }
	Color UnselectedColour { get; set; }
}

public interface IBrochureItem {
	RectTransform ItemTransform { get; set; }
	Vector2	AnchoredPosition { get; set;}
}

public abstract class BrochureTabAbstract : IBrochureTab {
	
	public Color SelectedColour { get; set; }
	public Color UnselectedColour { get; set; }

	public Animator TabAnim { get; set;	}
	
	public event BrochureAbstract.OnClickHandler OnClick;
	
	public void OnClickEvent ()
	{
		OnClick (this);
	}
	
	string categoryName;
	public string CategoryName { get { return categoryName; } }

	public BrochureTabAbstract(string _categoryname) {
		categoryName = _categoryname;
	}

}

public abstract class BrochureItemAbstract : IBrochureItem
{
	public abstract RectTransform ItemTransform { get; set; }
	public Vector2	AnchoredPosition { get; set;}
}

public abstract class BrochureAbstract : MonoBehaviour {

	[System.Serializable]
	public class CategoryDisplayData {
		public string m_category;
		public Color m_unselectedTabColour;
		public Color m_selectedTabColour;
		public Color m_pageDisplayColour;
		public Color m_pageItemDisplayColour;
	}

	[SerializeField] protected CanvasGroup m_cGroup;
	public float ItemVisiblePadding = 100.0f;
	[SerializeField] protected CategoryDisplayData[] categoryDisplayData;
	protected Dictionary<IBrochureTab, List<IBrochureItem>> m_sortedContent;
	protected RectTransform m_scrollContainer;
	[SerializeField] protected ScrollRect m_scroll;
	protected bool m_IsVisible { get; set; }
	protected Animator m_anim;
	protected IBrochureTab m_currentTab;
	[SerializeField] protected BrochureTab[] m_tabs;

	public delegate void OnClickHandler (IBrochureTab sender);

	protected void Awake () {
		m_anim = GetComponent<Animator> ();
		if (m_scroll == null)
			m_scroll = GetComponentInChildren <ScrollRect> ();
		m_scrollContainer = m_scroll.content;
	}

    protected void Start()
    {
        m_sortedContent = SortAllContent();
    }

	protected void SetVisible (bool isVisible) {

//        if (m_anim.GetBool("Show") != isVisible)
//		{
//			m_anim.SetBool("Show", isVisible);
//			IsVisible = isVisible;
//		}
		if (m_IsVisible != isVisible)
		{
			m_cGroup.alpha = isVisible ? 1f : 0f;
			m_cGroup.blocksRaycasts = isVisible;
			m_IsVisible = isVisible;
		}

        if (m_IsVisible)
            LoadContent(m_tabs[0].m_tabData);
	}

	protected void ClearContent () 
	{
		if (m_sortedContent != null && m_sortedContent.Count > 0 && m_currentTab != null)
		{
			List<IBrochureItem> contentList = null;
			if (m_sortedContent.TryGetValue(m_currentTab, out contentList))
			{
				foreach (IBrochureItem item in contentList)
				{
					if ((item.ItemTransform as RectTransform) != null)
					{
						ObjectPool.Me.PoolObject(item.ItemTransform.gameObject);
						item.ItemTransform = null;
					}
				}
			}
		}
	}

	protected virtual void LoadContent(IBrochureTab tab) {
		if (m_currentTab != null) {
			if (m_currentTab.TabAnim != null)
				m_currentTab.TabAnim.SetBool ("Selected", false);
		}
		ClearContent ();
		m_currentTab = tab;
		if (m_currentTab.TabAnim != null)
			m_currentTab.TabAnim.SetBool ("Selected", true);
		List<IBrochureItem> contentList = m_sortedContent[m_currentTab];
		CalculateItemPositions(contentList);
		
		m_scroll.velocity = Vector2.zero;
		m_scroll.verticalNormalizedPosition = 1f;
	}

	protected void SortTabs (Dictionary<IBrochureTab, List<IBrochureItem>> sortedContent) {
		int i = 0;
		foreach (var entry in sortedContent) {
			entry.Key.TabAnim = m_tabs[i].GetComponent<Animator> ();
			m_tabs[i].m_tabData = entry.Key;
			if (!TrySetCategoryDisplayData (entry.Key)) {
				Debug.LogWarning (string.Format ("Display data not correctly set up for brochure category: {0}", entry.Key.CategoryName));
				entry.Key.SelectedColour = Color.white;
				entry.Key.UnselectedColour = Color.grey;
			}
			if (!m_tabs[i].gameObject.activeInHierarchy)
				m_tabs[i].gameObject.SetActive (true);
			m_tabs[i].m_text.text = entry.Key.CategoryName;
			++i;
		}
		for (; i < m_tabs.Length; ++i)
			m_tabs [i].gameObject.SetActive (false);

	}

	bool TrySetCategoryDisplayData (IBrochureTab tab) {
		bool success = false;
		foreach (var data in categoryDisplayData) {
			if (!data.m_category.Equals (tab.CategoryName, System.StringComparison.CurrentCultureIgnoreCase))
				continue;
			tab.SelectedColour = data.m_selectedTabColour;
			tab.UnselectedColour = data.m_unselectedTabColour;
			success = true;
		}
		return success;
	}

	protected virtual void CreateAndSetupBrochureItemInstance(IBrochureItem item, int index)
	{
		item.ItemTransform  = GetNewItemObjectInstance(item).transform as RectTransform;
		item.ItemTransform = item.ItemTransform.transform as RectTransform;
		item.ItemTransform.SetParent (m_scrollContainer);
		item.ItemTransform.localScale = Vector3.one;
		item.ItemTransform.anchoredPosition = item.AnchoredPosition;
		FormatContent (item);
	}

	protected virtual void CalculateItemPositions(List<IBrochureItem> items) 
	{
		float totalHeight = 0.0f;
        int iElement = 0;
		foreach (IBrochureItem item in items)
		{
			var rect = GetNewItemObjectPrefab(item).transform as RectTransform;
			float height = rect.GetHeight() * rect.localScale.y;
			float yPos = -totalHeight;
            if (item.ItemTransform == null)
                CreateAndSetupBrochureItemInstance(item, iElement);
			item.ItemTransform.anchoredPosition = new Vector2 (0.0f, yPos);
			totalHeight += height;
            ++iElement;
		}
		m_scrollContainer.SetHeight (totalHeight);
	}

	protected abstract Dictionary<IBrochureTab, List<IBrochureItem>> SortAllContent ();
	protected GameObject GetNewItemObjectInstance (IBrochureItem item)
	{
		return ObjectPool.Me.AllocatePrefab(GetNewItemObjectPrefab(item), false);
	}
	protected abstract GameObject GetNewItemObjectPrefab (IBrochureItem item);
	protected abstract void FormatContent (IBrochureItem item);

	void Update () 
	{
		UpdateActiveObjects ();
	}

	void UpdateActiveObjects()
	{
		if (m_sortedContent != null && m_sortedContent.Count > 0 && m_currentTab != null)
		{
			List<IBrochureItem> contentList = null;
			if (m_sortedContent.TryGetValue(m_currentTab, out contentList))
			{
				int iElement = 0;
				foreach (IBrochureItem item in contentList)
				{
					bool show = IsItemOnScreen(item);
					var inst = item.ItemTransform as RectTransform;
					if (!show && inst != null)
					{
						ObjectPool.Me.PoolObject(item.ItemTransform.gameObject);
						item.ItemTransform = null;
					}
					else if (show && inst == null)
					{
						CreateAndSetupBrochureItemInstance(item, iElement);
					}
					iElement++;
				}
			}
		}
	}

	bool IsItemOnScreen(IBrochureItem item)
	{
		var scrollSize = m_scrollContainer.GetHeight();
		var p = Mathf.Clamp01(1.0f - m_scroll.verticalNormalizedPosition);
		var windowSize = ((RectTransform)m_scroll.transform).rect.height;
		var itemPrefab = GetNewItemObjectPrefab(item).transform as RectTransform;
		var itemYPos = -item.AnchoredPosition.y;
		var itemHeight = itemPrefab.GetHeight();
		var minScroll = scrollSize*p - (p)*windowSize - ItemVisiblePadding;
		var maxScroll = scrollSize*p + (1.0f - p)*windowSize + ItemVisiblePadding;
		return itemYPos + itemHeight >= minScroll && itemYPos <= maxScroll;
	}
}
