﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class BrochureContent : MonoBehaviour {

    public Text m_text;
	public Text m_info;
	public Text m_required;
	public Text m_energyCost;
    public Image m_image;
	public Image m_icon;
	public Button m_button;
    [HideInInspector] public Building m_building;

    public void Trigger_CreateNewBuilding()
    {
		Config.Me.m_gui.m_brochure.ToggleBrochure(false);
		Config.Me.m_grid.PlaceNewBuilding(m_building);
    }

}
