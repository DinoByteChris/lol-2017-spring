﻿using UnityEngine;
using System.Collections;
using System;
using System.Collections.Generic;

public class BuildingsTab : BrochureTabAbstract {

    public BuildingsTab(string _categoryName) : base(_categoryName) { }
}

public class BuildingsItem : BrochureItemAbstract
{
    public override RectTransform ItemTransform { get; set; }
    public Building BuildingData;
    public string DisplayName;
	public string Info;
	public string Requires;
    public Color DisplayColor;

    public BuildingsItem(Building data)
    {
        BuildingData = data;
		Info = data.Info;
		Requires = data.Requires;
        DisplayName = data.DisplayName;
        DisplayColor = data.DisplayColor;
    }
}

public class BuildingBrochure : BrochureAbstract {

    public void ToggleBrochure()
    {
        if (m_sortedContent == null || m_sortedContent.Count == 0)
            return;
        SetVisible (!m_IsVisible);
		Config.Me.m_gui.m_eHudState = m_IsVisible ? GUIController.E_HudState.building_menu : GUIController.E_HudState.none;
    }

	public void ToggleBrochure (bool isOpen)
	{
		SetVisible (isOpen);
		Config.Me.m_gui.m_eHudState = isOpen ? GUIController.E_HudState.building_menu : GUIController.E_HudState.none;
	}

    protected override void FormatContent(IBrochureItem item)
    {
        var buildingItem = item as BuildingsItem;
        var contentScript = item.ItemTransform.GetComponent<BrochureContent>();
        if (contentScript != null)
        {
			bool isAvailable = !buildingItem.BuildingData.IsLocked && buildingItem.BuildingData.IsAffordable;
			bool isLocked = !buildingItem.BuildingData.IsLocked;
			contentScript.m_button.interactable = isAvailable;
			contentScript.m_text.color = isAvailable ? Color.black : Color.grey;
            contentScript.m_text.text = buildingItem.DisplayName;
			contentScript.m_info.color = isAvailable ? Color.black : Color.grey;
			contentScript.m_info.text = buildingItem.Info;
			contentScript.m_required.color = isLocked ? Color.black : Color.red;
			contentScript.m_required.text =  isLocked ? null : buildingItem.Requires;
			contentScript.m_energyCost.color = isLocked ? (buildingItem.BuildingData.IsAffordable ? Color.black : Color.red) : Color.grey;
			contentScript.m_energyCost.text = buildingItem.BuildingData.DisplayEnergyString;
            contentScript.m_building = buildingItem.BuildingData;
			contentScript.m_icon.sprite = buildingItem.BuildingData.m_displaySprite;
			contentScript.m_image.color = isAvailable ? buildingItem.DisplayColor : buildingItem.DisplayColor = new Color (1f,1f,1f,5f);
        }
    }

    protected override GameObject GetNewItemObjectPrefab(IBrochureItem item)
    {
        return BuildingData.Me.m_prefabBuildingsGUI;
    }

    protected override Dictionary<IBrochureTab, List<IBrochureItem>> SortAllContent()
    {
        Dictionary<IBrochureTab, List<IBrochureItem>> dict = new Dictionary<IBrochureTab, List<IBrochureItem>>();
        var buildings = BuildingData.Me.Buildings;
        List<IBrochureItem> industrialBuildings = new List<IBrochureItem>();
        List<IBrochureItem> commercialBuildings = new List<IBrochureItem>();
		List<IBrochureItem> specialBuildings = new List<IBrochureItem> ();
        foreach (var building in buildings)
        {
            if (building == null)
                continue;
            if (building is CommercialBuilding)
				commercialBuildings.Add(new BuildingsItem(building));
            else if (building is IndustrialBuilding)
				industrialBuildings.Add(new BuildingsItem(building));
			else if (building is SpecialBuilding)
				specialBuildings.Add(new BuildingsItem(building));
            else
                throw new ArgumentOutOfRangeException();
        }
        var tab1 = new BuildingsTab ("Industry");
        var tab2 = new BuildingsTab ("Stores");
		var tab3 = new BuildingsTab ("Education");
        tab1.OnClick += base.LoadContent;
        tab2.OnClick += base.LoadContent;
		tab3.OnClick += base.LoadContent;

        dict.Add(tab1, industrialBuildings);
        dict.Add(tab2, commercialBuildings);
		dict.Add (tab3, specialBuildings);
        base.SortTabs(dict);
        m_currentTab = tab1;

        return dict;
    }

}
