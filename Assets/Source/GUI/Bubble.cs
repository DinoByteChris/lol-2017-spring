﻿using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections;

public class Bubble : MonoBehaviour {

	[SerializeField] private Text m_text;
	[SerializeField] private Button m_button;
	public static Bubble s_statusBubble;

	public static Bubble Create (string textIn, UnityAction action1 = null, UnityAction action2 = null) {
	
		Bubble bubble = Instantiate<Bubble> (Config.Me.m_gui.m_prefab_Bubble);
		bubble.transform.SetParent (Config.Me.m_gui.m_bubbleContainer);
		bubble.GetComponent<RectTransform> ().anchoredPosition = Vector2.zero;
		bubble.GetComponent<RectTransform> ().sizeDelta = Vector2.zero;
		bubble.m_text.text = textIn;
		if (action1 != null)
			bubble.m_button.onClick.AddListener (action1);
		if (action2 != null)
			bubble.m_button.onClick.AddListener (action2);
		Config.Me.m_audio.PlayClip (E_AudioClip.AUDIO_TUTORIAL);
		return bubble;

	}

	public static void CreateStatusBubble (string textIn)
	{
		s_statusBubble = Instantiate<Bubble> (Config.Me.m_gui.m_prefab_Bubble);
		s_statusBubble.transform.SetParent (Config.Me.m_gui.m_bubbleContainer);
		s_statusBubble.GetComponent<RectTransform> ().anchoredPosition = Vector2.zero;
		s_statusBubble.GetComponent<RectTransform> ().sizeDelta = Vector2.zero;
		s_statusBubble.m_text.text = textIn;
		Config.Me.m_audio.PlayClip (E_AudioClip.AUDIO_TUTORIAL);
		s_statusBubble.m_button.interactable = false;
	}

	public void Trigger_Acknowledge () {
		Destroy (this.gameObject);
	}


}
