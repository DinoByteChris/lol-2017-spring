﻿using UnityEngine;
using System.Collections;

public class PopupAnimState : StateMachineBehaviour {

    override public void OnStateExit(Animator animator, AnimatorStateInfo stateInfo, int layerIndex) {

        if (animator.transform.GetComponent<Popup>()) {
            var popScript = animator.transform.GetComponent<Popup>();
            Config.Me.m_gui.m_popupScript.RemovePopupInstance(popScript);
        }
	}
    
}
