﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlaceBuildingPopup : Popup {

	public GameObject m_confirm;

	public static PlaceBuildingPopup CreatePlacementPopup (BuildingVisual visual) {
		Vector2 pos = RectTransformUtility.WorldToScreenPoint (Camera.main, visual.transform.position);
		PlaceBuildingPopup newPop = Instantiate(Config.Me.m_gui.m_popupScript.m_prefabBuildingPopup);
		newPop.m_visual = visual;
		var rt = newPop.transform as RectTransform;
		rt.SetParent(Config.Me.m_gui.m_popupScript.transform);
		rt.anchoredPosition = pos;
		return newPop;
	}

	public void Trigger_Confirm ()
	{
		m_visual.ConfirmPlacement ();
		GetComponent<Animator> ().SetTrigger ("Close");
	}

}
