﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;

public class PauseScreen : MonoBehaviour {

	[SerializeField] private CanvasGroup m_cGroup;
	[SerializeField] private Text m_text;
	[SerializeField] private Text m_option1;
	[SerializeField] private Text m_option2;
	private Config.PopulationMilestone m_milestone;

	public void DisplayMilestone (Config.PopulationMilestone milestone) {

		Config.Me.m_audio.PlayClip (E_AudioClip.AUDIO_PROGRESS);

		m_milestone = milestone;
		m_text.text = milestone.m_text;
		m_cGroup.alpha = 1f;
		m_cGroup.blocksRaycasts = true;
		Config.Me.m_controller.ToggleGameTimeRate (false);

		FormatOptionText (milestone.m_choice1, m_option1);
		FormatOptionText (milestone.m_choice2, m_option2);

	}

	void FormatOptionText (BuildingEffects[] effects, Text button) {

		string s = string.Empty;
		foreach (var effect in effects) {
			switch (effect.m_eEffect) {
				case E_Effect.JOBS:
					s += "Jobs ";
					break;
				case E_Effect.AMENITIES:
					s += "Amenities ";
					break;
				case E_Effect.ENERGY:
					s += "Energy ";
					break;
				case E_Effect.BIOSPHERE:
					s += "Biosphere damage ";
					break;
				case E_Effect.POLLUTION:
					s += "Pollution ";
					break;
				case E_Effect.WASTE:
					s += "Waste ";
					break;
				case E_Effect.HAPPINESS:
					s += "Happiness ";
					break;
				default:
					throw new System.ArgumentOutOfRangeException ();
			}
			if (effect.m_value > 0)
				s += "+";
			s += effect.m_value;
			if (effect.m_eEffect != effects[effects.Length - 1].m_eEffect)
				s += "\r\n";
		}
		button.text = s;

	}

	public void Trigger_ResolveMilestoneOption1 () {
	
		EnactChanges (m_milestone.m_choice1);
		ResetCanvas ();
	
	}
		
	public void Trigger_ResolveMilestoneOption2 () {

		EnactChanges (m_milestone.m_choice2);
		ResetCanvas ();

	}

	void EnactChanges (BuildingEffects[] effects) {

		var currentGame = Config.Me.m_controller.m_game;
		foreach (var effect in effects) {
			switch (effect.m_eEffect) {
				case E_Effect.JOBS: 
					currentGame.m_jobs += Mathf.RoundToInt (effect.m_value);
					break;
				case E_Effect.AMENITIES:
					currentGame.m_amenities += Mathf.RoundToInt (effect.m_value);
					break;
				case E_Effect.ENERGY:
					currentGame.m_energyFromPopulation += effect.m_value;	
					currentGame.Energy += effect.m_value;
					break;
				case E_Effect.BIOSPHERE:
					currentGame.BiosphereDamage += effect.m_value;
					break;
				case E_Effect.POLLUTION:
					currentGame.Pollution += effect.m_value;
					break;
				case E_Effect.WASTE:
					currentGame.Waste += effect.m_value;
					break;
				case E_Effect.HAPPINESS:
					currentGame.Happiness += effect.m_value;
					break;
				default:
					break;
			}
		}

	}

	void ResetCanvas()
	{
		m_cGroup.alpha = 0f;
		m_cGroup.blocksRaycasts = false;
		Config.Me.m_controller.ToggleGameTimeRate (true);
	}

}
