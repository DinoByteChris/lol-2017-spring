﻿using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using LoLSDK;

public class GUIController : MonoBehaviour {

    public enum E_HudState { none, locked, building_menu }

    [HideInInspector] public E_HudState m_eHudState;
	public RectTransform m_startPanel;
	public RectTransform m_topBar;
	public RectTransform m_deathScreen;
	public RectTransform m_winScreen;
	public RectTransform m_bubbleContainer;
    public BuildingBrochure m_brochure;
    public PopupController m_popupScript;
	public PauseScreen m_pause;

	[SerializeField] private Text m_gameOverText;
	[SerializeField] private Text m_population;
	[SerializeField] private Text m_energy;
	[SerializeField] private Text m_footprint;
	[SerializeField] private Text m_happiness;
	[SerializeField] private Text m_waste;
	[SerializeField] private Text m_pollution;
	[SerializeField] private Text m_biosphere;
	[SerializeField] private Text m_buildingsEnergyValue;
	[SerializeField] private Text m_populationEnergyValue;
	[SerializeField] private Image m_energyIcon;
	[SerializeField] private Image m_FootprintIcon;
	[SerializeField] private Image m_HappinessIcon;
	[SerializeField] private Sprite m_happinessRed;
	[SerializeField] private Sprite m_happinessYellow;
	[SerializeField] private Sprite m_happinessGreen;
	[SerializeField] private Sprite m_footprintRed;
	[SerializeField] private Sprite m_footprintYellow;
	[SerializeField] private Sprite m_footprintGreen;

	public Bubble m_prefab_Bubble;

    public void Trigger_ToggleBuildingsMenu()
    {
		if (m_eHudState != E_HudState.locked) {
			Config.Me.m_audio.PlayClip (E_AudioClip.AUDIO_CLICK);
			m_brochure.ToggleBrochure ();
		}
    }

	public void Trigger_DestroyStructure () {

		if (Time.timeScale != 0) {
			if (Config.Me.m_grid.SelectedBuilding != null) {
//				Config.Me.m_grid.SelectedBuilding.RemoveFromGrid ();
				foreach (var space in Config.Me.m_grid.SelectedBuilding.Spaces) {
					if (space.building == Config.Me.m_grid.SelectedBuilding.m_building)
						space.building = null;
				}
				Destroy (Config.Me.m_grid.SelectedBuilding.gameObject);
			}
			else if (Config.Me.m_grid.m_selectionData != null)
			{
				Config.Me.m_grid.RemoveUnplacedBuilding ();
			}
		}
	}

	public void Trigger_StartGame ()
	{
		Config.Me.m_audio.PlayClip (E_AudioClip.AUDIO_CLICK);
		Config.Me.m_controller.NewGame ();
		m_startPanel.gameObject.SetActive (false);
		m_deathScreen.gameObject.SetActive (false);
		m_topBar.gameObject.SetActive (true);
	}

	public void SetGameOverScreen(bool diedByFootprint) {
	
		if (diedByFootprint) {
			m_gameOverText.text = "Sorry Mayor, but it looks like your carbon footprint was raised too much to continue to sustain the town. We will need to relocate elsewhere.";
		} else {
			m_gameOverText.text = "Sorry Mayor, but it looks like your citizens are unhappy and we cannot continue to sustain the town. We will need to relocate elsewhere.";
		}

	}

	public void GameOverScreen (bool toggleOn) {
	
		m_popupScript.RemoveAllPopups ();
		m_deathScreen.gameObject.SetActive (toggleOn);
		m_topBar.gameObject.SetActive (!toggleOn);

	}

	public void GameEndedScreen ()
	{
		m_topBar.gameObject.SetActive (false);
		m_winScreen.gameObject.SetActive (true);
	}

	public void Trigger_EndGame(){
		Config.Me.m_controller.StartQuiz ();
	}

	public void SetPopulation (int pop) {
		if (Config.Me == null || m_population == null)
			return;
		m_population.text = pop.ToString ();
	}

	public void SetEnergy () {
		if (Config.Me == null || m_energy == null)
			return;
		if (Config.Me.m_controller.m_game == null)
			return;
		m_energy.text = Config.Me.m_controller.m_game.EnergyToString;
		m_populationEnergyValue.text = Config.Me.m_controller.m_game.m_energyFromPopulation.ToString();
		m_buildingsEnergyValue.text = Config.Me.m_controller.m_game.m_energyFromBuildings.ToString();
	}

	public void SetFootprint () {

		StartCoroutine (WaitAndSetFootprint ());

	}

	IEnumerator WaitAndSetFootprint () {

		yield return null;

		if (Config.Me == null || m_footprint == null)
			yield break;
		if (Config.Me.m_gui == null || Config.Me.m_controller.m_game == null)
			yield break;

		m_footprint.text = Config.Me.m_controller.m_game.FootprintToString;
		Sprite sprite = new Sprite ();
		switch (Config.Me.m_controller.m_game.FootprintRating) {
			case GameController.Game.E_Footprint.GREEN:
				sprite = m_footprintGreen;
				break;
			case GameController.Game.E_Footprint.YELLOW:
				sprite = m_footprintYellow;
				break;
			case GameController.Game.E_Footprint.RED:
				sprite = m_footprintRed;
				break;
			default:
				throw new System.ArgumentOutOfRangeException ();
		}
		m_FootprintIcon.sprite = sprite;
		m_waste.text = Config.Me.m_controller.m_game.WasteToString;
		m_pollution.text = Config.Me.m_controller.m_game.PollutionToString;
		m_biosphere.text = Config.Me.m_controller.m_game.BiosphereToString;

		yield break;

	}

	public void SetHappiness () {

		if (Config.Me == null || m_happiness == null)
			return;
		if (Config.Me.m_controller.m_game == null)
			return;
		m_happiness.text = Config.Me.m_controller.m_game.HappinessToString;
		Sprite sprite = new Sprite ();
		switch (Config.Me.m_controller.m_game.HappinessRating) {
			case GameController.Game.E_Happiness.UNHAPPY:
				sprite = m_happinessRed;
				break;
			case GameController.Game.E_Happiness.NEUTRAL:
				sprite = m_happinessYellow;
				break;
			case GameController.Game.E_Happiness.HAPPY:
				sprite = m_happinessGreen;
				break;
			default:
				throw new System.ArgumentOutOfRangeException ();
		}
		m_HappinessIcon.sprite = sprite;

	}

}
