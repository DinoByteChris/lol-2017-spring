﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class PopupController : MonoBehaviour {

    [SerializeField] List<Popup> m_registeredPopups;
    public Popup m_prefabPopup;
	public PlaceBuildingPopup m_prefabBuildingPopup;

    void Awake()
    {
        m_registeredPopups = new List<Popup>();
    }

    public void CreatePopupInstance(BuildingVisual building)
    {
		if (Time.timeScale != 0f) {
			var newPop = Popup.Create (building);
			building.m_popup = newPop;
			newPop.m_text.text = building.m_building.DisplayName;
			m_registeredPopups.Add (newPop);
		}
    }

	public void CreateConfirmPopupInstance (BuildingVisual building)
	{
		if (Time.timeScale != 0f) {
			var newPop = PlaceBuildingPopup.CreatePlacementPopup (building);
			building.m_popup = newPop;
			newPop.m_text.text = building.m_building.DisplayName;
			m_registeredPopups.Add (newPop);
		}
	}

    public void RemovePopupInstance(Popup popup)
    {
        m_registeredPopups.Remove(popup);
        Destroy(popup.gameObject);
    }

	public void RemoveAllPopups()
	{
		if (m_registeredPopups.Count > 0) {
			RemovePopupInstance (m_registeredPopups [0]);
			if (m_registeredPopups.Count > 0)
				RemoveAllPopups ();
		}
	}

	void Update () {
		if (Config.Me.m_drag.IsDragging) {
			if (m_registeredPopups.Count > 0) {
				foreach (var popup in m_registeredPopups) {
					if (popup.m_visual != null) {
						var rt = popup.transform as RectTransform;
						rt.anchoredPosition = RectTransformUtility.WorldToScreenPoint (Camera.main, popup.m_visual.transform.position);
					}
				}
			}
		}
	}

}
