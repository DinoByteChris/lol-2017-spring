﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using LoLSDK;

public enum E_AudioClip
{
	AUDIO_CLICK,
	AUDIO_TUTORIAL,
	AUDIO_DESTROY,
	AUDIO_PROGRESS,
	AUDIO_BUILD
}

public class AudioManager : MonoBehaviour {

//	[SerializeField] private AudioSource m_click;
//	[SerializeField] private AudioSource m_tutorial;
//	[SerializeField] private AudioSource m_destroy;
//	[SerializeField] private AudioSource m_progress;
//	[SerializeField] private AudioSource m_build;

	public void PlayMusic () {
	
		int sceneId = SceneManager.GetActiveScene ().buildIndex;
		if (sceneId == 0) {
			LOLSDK.Instance.PlaySound ("Easy_Lemon_60_Second.mp3", true, true);
		} else if (sceneId == 1) {
			LOLSDK.Instance.StopSound ("Easy_Lemon_60_Second.mp3");
			LOLSDK.Instance.PlaySound ("Home_Base_Groove.mp3", true, true);
		}
	
	}

	public void PlayClip (E_AudioClip clip) {

		/* 
		Build.wav
		destroy_tool.wav
		Easy_Lemon_60_Second.mp3
		Home_Base_Groove.mp3
		pop_Button_Clicks.wav
		Pop_Help_Text_Tutorial.wav
		Progress_Report_Popup.mp3
		*/

		if (LOLSDK.Instance != null) {
			switch (clip) {
				case E_AudioClip.AUDIO_CLICK:
					LOLSDK.Instance.PlaySound ("pop_Button_Clicks.wav");
//					m_click.Play ();
					break;
				case E_AudioClip.AUDIO_TUTORIAL:
					LOLSDK.Instance.PlaySound ("Pop_Help_Text_Tutorial.wav");
//					m_tutorial.Play ();
					break;
				case E_AudioClip.AUDIO_DESTROY:
					LOLSDK.Instance.PlaySound ("destroy_tool.wav");
//					m_destroy.Play ();
					break;
				case E_AudioClip.AUDIO_PROGRESS:
					LOLSDK.Instance.PlaySound ("Progress_Report_Popup.mp3");
//					m_progress.Play ();
					break;
				case E_AudioClip.AUDIO_BUILD:
					LOLSDK.Instance.PlaySound ("Build.wav");
//					m_build.Play ();
					break;
				default:
					throw new System.ArgumentOutOfRangeException ();
			}
		}
	}

}
