﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ParticleSortLayer : MonoBehaviour {

	void Awake () {

		var rend = GetComponent<ParticleSystemRenderer> ();
		if (rend != null) {
			rend.sortingLayerName = "Building";
		}
		
	}

}
