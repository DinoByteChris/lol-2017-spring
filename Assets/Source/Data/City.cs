﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class City : Building {

	public Sprite[] m_sprites = new Sprite[3];

	public override string DisplayName {
		get {
			return "Greenville";
		}
	}

	public override Color DisplayColor {
		get {
			return new Color (0f, 0f, 0f, 0f);
		}
	}

	public override bool IsLocked {
		get {
			return true;
		}
	}

	public override string Info { get { return "Our fair City"; } }

	public override string Requires { get { return "Good management"; } }

}
