﻿using UnityEngine;
using System.Collections;
using System;

public class SpecialBuilding : Building
{
    [SerializeField]
    private string m_displayName;
	[SerializeField]
	private string m_info;
	[SerializeField]
	private string m_requires;
    [SerializeField]
    private Color m_displayColor;
	public int m_unlockedBy = 0;

	public override bool IsLocked { get { return m_unlockedBy == 0 ? false : Config.Me.m_controller.m_game.Population < m_unlockedBy; } }
    public override string DisplayName { get { return m_displayName; } }
	public override string Info { get { return m_info; } }
	public override string Requires { get { return m_requires; } }
    public override Color DisplayColor { get { return m_displayColor; } }

}
