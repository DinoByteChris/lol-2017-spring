﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System;
using System.Linq;

[DisallowMultipleComponent]
public abstract class Building : MonoBehaviour
{
    public abstract string DisplayName { get; }
    public abstract Color DisplayColor { get; }
	public abstract string Info { get; }
	public abstract string Requires { get; }
	public E_TerrainType m_eTerrain;
	public int EnergyCost {
		get {
			foreach (var effect in m_effects) {
				if (effect.m_eEffect == E_Effect.ENERGY)
					return effect.m_value;
			}
			return 0;
		}
	}
	public string DisplayEnergyString {
		get 
		{
			return string.Format ("Energy: {0}", EnergyCost);
		}
	}
	public bool IsAffordable {
		get 
		{ 
			if (Config.Me != null) {
				if (Config.Me.m_controller.m_game != null)
					return EnergyCost > 0 ? true : -EnergyCost <= Config.Me.m_controller.m_game.Energy;
			}
			return false;
		}
	}
	public abstract bool IsLocked { get; }
	public int GridWidth {
		get
		{
			switch (m_eGridSize) {
				case E_BuildingSize.Building_1x1:
					return 1;
				case E_BuildingSize.Building_2x2:
					return 2;
				case E_BuildingSize.Building_3x3:
				case E_BuildingSize.Building_3x4:
				case E_BuildingSize.Building_3x5:
					return 3;
				case E_BuildingSize.Building_4x2:
				case E_BuildingSize.Building_4x3:
				case E_BuildingSize.Building_4x4:
					return 4;
				case E_BuildingSize.Building_5x4:
				case E_BuildingSize.Building_5x5:
					return 5;
				case E_BuildingSize.City_9x9:
					return 9;
				default:
					throw new ArgumentOutOfRangeException ();
			}
		}
	}

	public int GridHeight {
		get
		{
			switch (m_eGridSize) {
			case E_BuildingSize.Building_1x1:
				return 1;
			case E_BuildingSize.Building_2x2:
			case E_BuildingSize.Building_4x2:
				return 2;
			case E_BuildingSize.Building_3x3:
			case E_BuildingSize.Building_4x3:
				return 3;
			case E_BuildingSize.Building_3x4:
			case E_BuildingSize.Building_4x4:
			case E_BuildingSize.Building_5x4:
				return 4;
			case E_BuildingSize.Building_3x5:
			case E_BuildingSize.Building_5x5:
				return 5;
			case E_BuildingSize.City_9x9:
				return 9;
			default:
				throw new ArgumentOutOfRangeException ();
			}
		}
	}

	public Sprite m_displaySprite;

	public E_BuildingSize m_eGridSize = E_BuildingSize.Building_1x1;
	public BuildingEffects[] m_effects;

	public void EnactChanges () {
	
		var currentGame = Config.Me.m_controller.m_game;
		foreach (var effect in m_effects) {
			switch (effect.m_eEffect) {
			case E_Effect.JOBS: 
				currentGame.m_jobs += Mathf.RoundToInt (effect.m_value);
				break;
			case E_Effect.AMENITIES:
				currentGame.m_amenities += Mathf.RoundToInt (effect.m_value);
				break;
			case E_Effect.ENERGY:
				currentGame.m_energyFromBuildings += effect.m_value;
				currentGame.Energy += effect.m_value;
				break;
			case E_Effect.BIOSPHERE:
				currentGame.BiosphereDamage += effect.m_value;
				break;
			case E_Effect.POLLUTION:
				currentGame.Pollution += effect.m_value;
				break;
			case E_Effect.WASTE:
				currentGame.Waste += effect.m_value;
				break;
			case E_Effect.HAPPINESS:
				currentGame.Happiness += effect.m_value;
				break;
			default:
				throw new ArgumentOutOfRangeException ();
			}
		}
	
	}

	public void ReverseChanges() {

		if (Config.Me == null)
			return;
		var currentGame = Config.Me.m_controller.m_game;
		if (currentGame == null)
			return;

		foreach (var effect in m_effects) {
			switch (effect.m_eEffect) {
			case E_Effect.JOBS: 
				currentGame.m_jobs -= Mathf.RoundToInt (effect.m_value);
				break;
			case E_Effect.AMENITIES:
				currentGame.m_amenities -= Mathf.RoundToInt (effect.m_value);
				break;
			case E_Effect.ENERGY:
				currentGame.m_energyFromBuildings -= effect.m_value;
				currentGame.Energy -= effect.m_value;
				break;
			case E_Effect.BIOSPHERE:
				currentGame.BiosphereDamage -= effect.m_value;
				break;
			case E_Effect.POLLUTION:
				currentGame.Pollution -= effect.m_value;
				break;
			case E_Effect.WASTE:
				currentGame.Waste -= effect.m_value;
				break;
			case E_Effect.HAPPINESS:
				currentGame.Happiness -= effect.m_value;
				break;
			default:
				throw new ArgumentOutOfRangeException ();
			}
		}
	}

}

public enum E_BuildingSize
{
	Building_1x1,
	Building_2x2,
	Building_3x3,
	Building_3x4,
	Building_3x5,
	Building_4x4,
	Building_4x2,
	Building_4x3,
	Building_5x4,
	Building_5x5,
	City_9x9
}

public enum E_Effect {
	
	JOBS,
	AMENITIES,
	ENERGY,
	BIOSPHERE,
	POLLUTION,
	WASTE,
	HAPPINESS

}

[Serializable]
public struct BuildingEffects {

	public E_Effect m_eEffect;
	public int m_value;

}

public class BuildingData : MonoSingleton<BuildingData> {

    public GameObject m_prefabBuildingsGUI;
	[SerializeField] private Building[] m_buildings;
	[SerializeField] private Obstacle[] m_obstacles;
	public City m_city;

    public Building[] Buildings { get { return m_buildings; } }

	public Obstacle[] Obstacles { get { return m_obstacles; } }

    public int GetIndexForBuilding (Building building)
    {
        return Array.IndexOf(m_buildings, building);
    }

	public int[] GetPopulationUnlockNumbers (){

		return Buildings.OfType<SpecialBuilding> ().OrderBy (x => x.m_unlockedBy).Select (x => x.m_unlockedBy).ToArray ();

	}

    public Building GetBuilding(string name)
    {
        foreach (var b in m_buildings)
        {
            if (b.DisplayName == name)
                return b;
        }
        return null;   
    }

}
