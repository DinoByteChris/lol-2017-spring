﻿using UnityEngine;
using System.Collections;

public class Obstacle : Building {

	public override string DisplayName { get { return "Obstacle"; }	}
	public override Color DisplayColor { get { return new Color (0f, 0f, 0f, 0f); } }
	public override bool IsLocked { get { return true; } }

	public override string Info { get { return "A destructible obstruction"; } }

	public override string Requires { get { return "Tender love and care"; } }
		
}
