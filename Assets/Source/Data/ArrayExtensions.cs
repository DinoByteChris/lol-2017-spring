﻿using System;
using System.Linq;

public static class ArrayExtensions {

    public static int[] FindIndex(this Array array, object element)
    {
        if (array.Rank == 1)
            return new[] { Array.IndexOf(array, element) };

        var found = array.OfType<object>()
                            .Select((v, i) => new { v, i })
                            .FirstOrDefault(s => s.v.Equals(element));
        if (found == null)
            throw new Exception("Object not found in set.");

        var indexes = new int[array.Rank];
        var last = found.i;
        var lastLength = Enumerable.Range(0, array.Rank)
                                    .Aggregate(1,
                                        (a, v) => a * array.GetLength(v));
        for (var rank = 0; rank < array.Rank; rank++)
        {
            lastLength = lastLength / array.GetLength(rank);
            var value = last / lastLength;
            last -= value * lastLength;

            var index = value + array.GetLowerBound(rank);
            if (index > array.GetUpperBound(rank))
                throw new IndexOutOfRangeException();
            indexes[rank] = index;
        }

        return indexes;
    }

}
