﻿using UnityEngine;
using System.Collections;
using System;

public class IndustrialBuilding : Building
{
    [SerializeField]
    private string m_displayName;
	[SerializeField]
	private string m_info;
	[SerializeField]
	private string m_requires;
    [SerializeField]
    private Color m_displayColor;
	public Building m_unlockedBy;

	public override bool IsLocked { get { return m_unlockedBy == null ? false : !Config.Me.m_grid.m_constructedBuildings.Contains (m_unlockedBy); } }
    public override string DisplayName { get { return m_displayName; } }
	public override string Info { get { return m_info; } }
	public override string Requires { get { return m_requires; } }
    public override Color DisplayColor { get { return m_displayColor; } }

}
