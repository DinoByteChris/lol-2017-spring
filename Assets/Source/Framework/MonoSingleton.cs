﻿using UnityEngine;

public class MonoSingleton<T> : MonoBehaviour where T : MonoSingleton<T>
{
	protected static T s_Me;

	virtual protected void _Awake(){}
	virtual protected void _OnEnable(){}
	virtual protected void _OnDestroy(){}

	public static T Me { get { return s_Me;	} }

	private bool SetupInstance() {
		if (s_Me == this)
			return true;
		if (s_Me == null) {
			s_Me = this as T;
			return true;
		} 
		else {
			Debug.LogError (string.Format ("DOUBLE SINGLETON. KILLING SECOND INSTANCE ({0})", GetType()));
			enabled = false;
			return false;
		}
	}

	protected virtual void Awake() {
		if (SetupInstance())
			_Awake();
	}

	protected void OnEnable() {
		if (SetupInstance())
			_OnEnable();
	}

	protected void OnDestroy() {
		if (s_Me == this) {
			_OnDestroy();
			Destroy(s_Me);
			s_Me = null;
		}
	}
}
