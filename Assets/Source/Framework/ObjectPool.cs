﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

public class ObjectPool : MonoSingleton<ObjectPool>
{
	struct AllocateData
	{
		private string 		m_resPath;
		private GameObject	m_prefab;
		
		public string 		m_name {get;set;}
		
		public GameObject 	m_Prefab {
			get {
				if (m_prefab != null)
					return m_prefab;
				if (m_resPath != null)
				{	
					m_prefab = Resources.Load<GameObject>(m_resPath);
					return m_prefab;
				}
				Debug.LogError("Cannot get prefab with resPath or prefab ref");
				return null;
			}
			set {m_prefab = value;}
		}
		public string 		m_ResPath {
			get { return m_resPath;}
			set { 
				m_resPath = value;
				if (m_resPath != null)
				{
					var split = m_resPath.Split(new char[]{'/'});
					m_name = split[split.Length -1];
				}
			}
		}
		
		public bool Matches(GameObject pooledType)
		{
			return (pooledType == m_prefab || (pooledType != null && pooledType.name == m_name));
		}
	}

	/// <summary>
	/// The object prefabs which the pool can handle.
	/// </summary>
	public List<GameObject> objectPrefabs;
	
	/// <summary>
	/// The pooled objects currently available.
	/// </summary>
	public List<List<GameObject>> pooledObjects;
	
	/// <summary>
	/// The amount of objects of each type to buffer.
	/// </summary>
	public int[] amountToBuffer;
	
	public int defaultBufferAmount = 3;

	protected override void _Awake ()
	{
		//Loop through the object prefabs and make a new list for each one.
		//We do this because the pool can only support prefabs set to it in the editor,
		//so we can assume the lists of pooled objects are in the same order as object prefabs in the array
		pooledObjects = new List<List<GameObject>>();
		
		int i = 0;
		foreach ( GameObject objectPrefab in objectPrefabs )
		{
			pooledObjects.Add(new List<GameObject>()); 
			
			int bufferAmount;
			
			if(i < amountToBuffer.Length) bufferAmount = amountToBuffer[i];
			else
				bufferAmount = defaultBufferAmount;

			for ( int n=0; n<bufferAmount; n++)
			{
				GameObject newObj = Instantiate(objectPrefab) as GameObject;
				newObj.name = objectPrefab.name;
				PoolObject(newObj);
			}
			
			i++;
		}
	}

	public GameObject GetPrefabForResPath(string resPath, bool addIfNotPresent = true)
	{
		var ad = new AllocateData() {m_ResPath = resPath};
		return GetPrefabForType(ad, addIfNotPresent);
	}

	GameObject GetPrefabForType(AllocateData allocateData, bool addIfNotPresent = true)
	{
		int found = GetPrefabIndexForType(allocateData, addIfNotPresent);
		if (found != -1)
			return objectPrefabs[found];
		return null;
	}

	int GetPrefabIndexForType(AllocateData allocateData, bool addIfNotPresent = true)
	{
		for(int i=0; i<objectPrefabs.Count; i++)
		{
			GameObject prefab = objectPrefabs[i];
			if(allocateData.Matches(prefab))
			{
				return i;
			}
		}
		if (addIfNotPresent)
		{
			var newObject = allocateData.m_Prefab;
			if (newObject != null)
			{
				objectPrefabs.Add(newObject);
				pooledObjects.Add(new List<GameObject>());
				return objectPrefabs.Count - 1;
			}
			else
			{
				Debug.Log(string.Format("Could not pool null object for objectType {0}", allocateData));
				return -1;
			}
		}
		return -1;
	}
	
	public GameObject AllocatePrefab ( GameObject prefabType , bool onlyPooled, bool addIfNotPresent = true)
	{
		var ad = new AllocateData() {m_Prefab = prefabType};
		return Allocate(ad, onlyPooled, addIfNotPresent);
	}

	public GameObject AllocateByName ( string objectName , bool onlyPooled, bool addIfNotPresent = true)
	{
		var ad = new AllocateData() {m_name = objectName};
		return Allocate(ad, onlyPooled, addIfNotPresent);
	}

	/// <summary>
	/// Gets a new object for the name type provided.  If no object type exists or if onlypooled is true and there is no objects of that type in the pool
	/// then null will be returned.
	/// </summary>
	/// <returns>
	/// The object for type.
	/// </returns>
	/// <param name='onlyPooled'>
	/// If true, it will only return an object if there is one currently pooled.
	/// </param>
	/// <param name='addIfNotPresent'>
	/// Adds this object type to the pool if not already added
	/// </param>
	public GameObject AllocateByResPath ( string resPath , bool onlyPooled, bool addIfNotPresent = true)
	{
		var ad = new AllocateData() {m_ResPath = resPath};
		return Allocate(ad, onlyPooled, addIfNotPresent);
	}

	GameObject Allocate (AllocateData allocateData, bool onlyPooled, bool addIfNotPresent = true)
	{
		int prefabIndex = GetPrefabIndexForType(allocateData, addIfNotPresent);

		if(pooledObjects[prefabIndex].Count > 0)
		{
			GameObject pooledObject = pooledObjects[prefabIndex][0];
			pooledObjects[prefabIndex].RemoveAt(0);
			pooledObject.transform.SetParent(null);
			pooledObject.SetActive(true);
			return pooledObject;
			
		} else if(!onlyPooled) {
			if (objectPrefabs[prefabIndex] == null) return null;
			return Instantiate(objectPrefabs[prefabIndex]) as GameObject;
		}		
		
		//If we have gotten here either there was no object of the specified type or non were left in the pool with onlyPooled set to true
		return null;
	}
	
	/// <summary>
	/// Pools the object specified.  Will not be pooled if there is no prefab of that type.
	/// </summary>
	/// <param name='obj'>
	/// Object to be pooled.
	/// </param>
	public bool PoolObject ( GameObject obj )
	{
		if (obj == null) return false;
		string[] name = obj.name.Split('('); //split TrailItem(clone)
		for ( int i=0; i<objectPrefabs.Count; i++)
		{
			if (objectPrefabs[i] == null) continue;
			if(objectPrefabs[i].name == name[0])
			{
				obj.SetActive(false);
				obj.transform.SetParent(this.transform);
				pooledObjects[i].Add(obj);
				return true;
			}
		}
		return false;
	}
	
}

