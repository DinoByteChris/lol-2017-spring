﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.EventSystems;

public class Drag : MonoBehaviour, IBeginDragHandler, IDragHandler, IEndDragHandler  {

	public float m_dragSpeed = 5f;
	[HideInInspector] public float m_minXPos;
	[HideInInspector] public float m_maxXPos;
	[HideInInspector] public float m_minYPos;
	[HideInInspector] public float m_maxYPos;
	private Vector3 m_camStartPos;
	private Vector3 m_initialHitPos;
	private Vector3 m_dragPos;
	private bool m_isDragging;
	public bool IsDragging { get { return m_isDragging; } }

	void FixedUpdate ()
	{
		if (m_isDragging) {
			var pos = Vector3.Lerp (Camera.main.transform.position, m_camStartPos + m_dragPos, Time.deltaTime * m_dragSpeed);
			Config.Me.m_mainCamera.transform.position = new Vector3 (pos.x, Config.Me.m_mainCamera.transform.position.y, pos.z);
		}
	}

	public void OnBeginDrag (PointerEventData eventData)
	{
		m_isDragging = true;
		m_camStartPos = Config.Me.m_mainCamera.position;
		m_initialHitPos = GetPointerHitPosition (eventData);
	}

	public void OnDrag (PointerEventData eventData)
	{
		if (m_isDragging) {
			var hitPos = GetPointerHitPosition (eventData);
			m_dragPos = m_initialHitPos - hitPos;
		}
	}

	public void OnEndDrag (PointerEventData eventData)
	{
		m_isDragging = false;
	}

	public static Vector3 GetPointerHitPosition (PointerEventData eventData)
	{
		Vector3 point = Vector3.zero;
		var pos = eventData.position;
		var origin = new Vector3 (pos.x, pos.y, 0f);
		RaycastHit hit;
		if (Physics.Raycast (Camera.main.ScreenPointToRay (origin), out hit, LayerMask.GetMask ("GroundPlane")))
			point = hit.point;
		return point;
	}

}
