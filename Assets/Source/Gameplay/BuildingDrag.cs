﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.EventSystems;

public class BuildingDrag : MonoBehaviour, IPointerClickHandler, IBeginDragHandler, IDragHandler {

	[SerializeField] private BuildingVisual m_visual;

	void Awake ()
	{
		if (m_visual == null)
			m_visual = GetComponentInParent<BuildingVisual> ();
	}

	public void OnPointerClick (PointerEventData eventData)
	{
		if (m_visual is CityVisual)
			return;

		if (m_visual.m_isPlaced) {
			if (Config.Me.m_grid.SelectedBuilding == m_visual)
				Config.Me.m_grid.SelectedBuilding = null;
			else {
				Config.Me.m_grid.SelectedBuilding = m_visual;
				m_visual.m_anim.SetBool ("Selected", true);
				Config.Me.m_gui.m_popupScript.CreatePopupInstance (m_visual);
			}
		}
	}

	public void OnBeginDrag (PointerEventData eventData)
	{
		if (!m_visual.m_isPlaced)
			Config.Me.m_grid.m_draggedBuilding = m_visual;
	}

	public void OnDrag (PointerEventData eventData)
	{
		if (!m_visual.m_isPlaced) {
			var hitPos = Drag.GetPointerHitPosition (eventData);
			var space = Grid.Test_GetClosestGridSpace (hitPos);
			m_visual.Spaces = Grid.GetCurrentlySelectedSpaces (space);
			if (m_visual.m_popup != null) {
				RectTransform rt = m_visual.m_popup.transform as RectTransform;
				var pos = RectTransformUtility.WorldToScreenPoint (Camera.main, transform.position);
				rt.anchoredPosition = pos;
				if (m_visual.m_popup is PlaceBuildingPopup) {
					bool valid = Grid.Test_IsValidBuildingSelection (m_visual.Spaces);
					var pop = m_visual.m_popup as PlaceBuildingPopup;
					pop.m_confirm.SetActive (valid);
				}
			}
			if (eventData.position.y > Screen.height - 200) {
				Vector3 camPos = Config.Me.m_mainCamera.transform.position;
				Config.Me.m_mainCamera.transform.position = new Vector3 (camPos.x, camPos.y, camPos.z + 0.5f);
			} else if (eventData.position.y < 120) {
				Vector3 camPos = Config.Me.m_mainCamera.transform.position;
				Config.Me.m_mainCamera.transform.position = new Vector3 (camPos.x, camPos.y, camPos.z - 0.5f);
			}
			if (eventData.position.x > Screen.width - 120) {
				Vector3 camPos = Config.Me.m_mainCamera.transform.position;
				Config.Me.m_mainCamera.transform.position = new Vector3 (camPos.x + 0.5f, camPos.y, camPos.z);
			} else if (eventData.position.x < 40) {
				Vector3 camPos = Config.Me.m_mainCamera.transform.position;
				Config.Me.m_mainCamera.transform.position = new Vector3 (camPos.x - 0.5f, camPos.y, camPos.z);
			}
		}
	}

}
