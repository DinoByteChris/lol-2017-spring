﻿using UnityEngine;
using UnityEngine.SceneManagement;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using LoLSDK;

public delegate void ValueChangedHandler (int oldValue, int newValue);

public class GameController : MonoBehaviour {

	public class Game {

		public event ValueChangedHandler OnFootprintChanged;
		public event ValueChangedHandler OnEnergyChanged;
		public event ValueChangedHandler OnPopulationChanged;

		public enum E_Happiness { UNHAPPY, NEUTRAL, HAPPY }
		public enum E_Footprint { GREEN, YELLOW, RED }
		public enum E_Energy { GREEN, RED }

		public float m_time;
		private int m_phase;
		public int m_milestone;
		public int m_tutorialCount;
		private int[] m_unlockPopulations;
		private int m_unlocks;
		private int CurrentUnlockToEarn { get { return m_unlockPopulations != null && m_unlocks < m_unlockPopulations.Length ? m_unlockPopulations [m_unlocks] : -1; } }
		public List<Building> m_currentRequiredBuildings;
		public int m_energyFromBuildings;
		public int m_energyFromPopulation;
		[Range(0, 100)] private int m_happiness;
		[Range(0, 100)] private int m_footprint;
		private int m_energy;

		public int Population 
		{ 
			get 
			{ 
				if (Config.Me.m_exponentialPopulationGrowth)
					return Mathf.RoundToInt (Config.Me.m_startPopulation * Mathf.Pow (Config.Me.PopulationMultiplier, (float)m_phase));
				else
					return Mathf.RoundToInt (Config.Me.m_startPopulation + ((Config.Me.m_endPopulation - Config.Me.m_startPopulation) / Config.Me.NoOfPhases) * (float) m_phase);
			}
		}

		public int GetNextPopulationBounce () {
		
			if (Config.Me.m_exponentialPopulationGrowth)
				return Mathf.RoundToInt (Config.Me.m_startPopulation * Mathf.Pow (Config.Me.PopulationMultiplier, (float)++m_phase));
			else
				return Mathf.RoundToInt (Config.Me.m_startPopulation + ((Config.Me.m_endPopulation - Config.Me.m_startPopulation) / Config.Me.NoOfPhases) * (float) (m_phase + 1));
		
		}

		public int Happiness { get { return m_happiness; }
			set
			{
				m_happiness = Mathf.Clamp (value, 0, 100);
				if (m_happiness <= 0) {
					Config.Me.m_gui.SetGameOverScreen (false);
					Config.Me.m_controller.PlayerDied ();
				}
				Config.Me.m_gui.SetHappiness ();
			}
		}

		public int Footprint {
			get	{
				return m_footprint;
			}
			set	{
				OnFootprintChanged (m_footprint, value);
				m_footprint = value;
				if (Config.Me.m_gui != null)
					Config.Me.m_gui.SetFootprint ();
			}
		}

		public int Energy {
			get	{
				return m_energy;
			}
			set	{
				OnEnergyChanged (m_energy, value);
				m_energy = value;
				Config.Me.m_gui.SetEnergy ();
			}
		}

		private int m_pollution;

		public int Pollution {
			get {
				return m_pollution;
			}
			set {
				Footprint += (value - m_pollution);
				m_pollution = value;
			}
		}

		public string PollutionToString { get { return string.Format ("{0}%", m_pollution); } }

		private int m_waste;

		public int Waste {
			get {
				return m_waste;
			}
			set {
				Footprint += (value - m_waste);
				m_waste = value;
			}
		}

		public string WasteToString { get { return string.Format ("{0}%", m_waste); } }

		private int m_biosphereDamage;

		public int BiosphereDamage {
			get {
				return m_biosphereDamage;
			}
			set {
				Footprint += (value - m_biosphereDamage);
				m_biosphereDamage = value;
			}
		}

		public string BiosphereToString { get { return string.Format ("{0}%", m_biosphereDamage); } }

		public int m_jobs;
		public int m_amenities;
		public int JobsAndAmenities { get { return m_jobs + m_amenities; } } 

		public E_Happiness HappinessRating {
			get {
				if (m_happiness <= 30)
					return E_Happiness.UNHAPPY; // :(
				else if (m_happiness >= 71)
					return E_Happiness.HAPPY; // :)
				else
					return E_Happiness.NEUTRAL;
			}
		}
		public string HappinessToString { get { return string.Format ("{0}%", m_happiness); } }

		public E_Footprint FootprintRating {
			get { 
				if (m_footprint <= 29)
					return E_Footprint.GREEN; // :(
				else if (m_footprint >= 70)
					return E_Footprint.RED; // :)
				else
					return E_Footprint.YELLOW;
			}
		}
		public string FootprintToString { get { return string.Format ("{0}%", m_footprint); } }

		public string EnergyToString { get { return m_energy.ToString(); } }

		public Game () {
			m_time = Time.time;
			m_phase = 0;
			m_happiness = 50;
			m_footprint = 0;
			m_pollution = 0;
			m_waste = 0;
			m_biosphereDamage = 0;
			m_energy = 0;
			m_milestone = 0;
			m_unlockPopulations = BuildingData.Me.GetPopulationUnlockNumbers();
			m_unlocks = 0;
			m_currentRequiredBuildings = new List<Building>();
		}

		public static Game StartGame ()	{
			return new Game ();
		}

		public bool IsPhaseOver () {

			if (m_currentRequiredBuildings.Count == 0 && Bubble.s_statusBubble != null)
				Bubble.s_statusBubble.Trigger_Acknowledge ();
			if (Population >= CurrentUnlockToEarn) {
				++m_unlocks;
			}
			bool unlockRelevant = CurrentUnlockToEarn > 0 ? GetNextPopulationBounce() >= CurrentUnlockToEarn : false;
			bool blockedByUnlock = false;
			if (unlockRelevant) {
				List<string> unbuilt = new List<string> ();
				foreach (var b in BuildingData.Me.Buildings) {
					if (b.IsLocked)
						continue;
					if (!Config.Me.m_grid.m_constructedBuildings.Contains (b)) {
						if (!m_currentRequiredBuildings.Contains (b) && Bubble.s_statusBubble == null) {
							m_currentRequiredBuildings.Add (b);
							unbuilt.Add (b.DisplayName);
						}
						blockedByUnlock = true;
					}
				}
				if (Bubble.s_statusBubble == null && blockedByUnlock) {
					string s = string.Empty;
					if (unbuilt.Count == 1)
						s += unbuilt [0];
					else if (unbuilt.Count == 2)
						s = unbuilt [0] + " and " + unbuilt [1];
					else {
						foreach (var n in unbuilt) {
							if (n != unbuilt [unbuilt.Count - 1])
								s += n + ", ";
							else
								s += "and " + n;
						}
					}
					Bubble.CreateStatusBubble (string.Format ("We need to build the {0} if we want the town to keep growing!", s));
				}
			}

			bool timePassed = Time.time >= m_time + Config.Me.m_timeToNextPhase;
			bool over = !blockedByUnlock && timePassed;

			return over;
		}

		public void EndPhase() {
			int oldPopulation = Population;
			++m_phase;
			m_time = Time.time;
			OnPopulationChanged (oldPopulation, Population);
		}

	}

	public Game m_game;

	public void NewGame () {

		m_game = Game.StartGame ();
		var gui = Config.Me.m_gui;
		gui.SetPopulation (m_game.Population);
		LOLSDK.Instance.SubmitProgress (0, 1, 16);
		DisplayTutorialBubble ();

		gui.SetEnergy ();
		gui.SetFootprint ();
		gui.SetHappiness ();
		m_game.OnFootprintChanged += FootprintChanged;
		m_game.OnEnergyChanged += EnergyChanged;
		m_game.OnPopulationChanged += PopulationChanged;
	}

	public void PlayerDied () {

		if (Bubble.s_statusBubble != null)
			Bubble.s_statusBubble.Trigger_Acknowledge ();
		var buildings = FindObjectsOfType<BuildingVisual> ();
		foreach (var b in buildings) {
			Destroy (b.gameObject);
		}
//		var gridSquares = FindObjectsOfType<GridBehaviour> ();
//		foreach (var s in gridSquares) {
//			Destroy (s.gameObject);
//		}
		Config.Me.m_grid.m_constructedBuildings.Clear ();
		Config.Me.m_grid.SetupGrid ();
		m_game = null;
		Config.Me.m_gui.GameOverScreen (true);

	}

	public void StartQuiz()
	{
		SceneManager.LoadScene (1);
	}

	void FootprintChanged (int oldValue, int newValue) {

		if (newValue >= 100) {
			Config.Me.m_gui.SetGameOverScreen (true);
			PlayerDied ();
			return;
		}

		if (m_game == null)
			return;

		// TODO: not hard-coded
		if (oldValue < newValue) { // increased
			if (oldValue < 40 && newValue >= 40)
				m_game.Happiness -= 10;
			if (oldValue < 50 && newValue >= 50)
				m_game.Happiness -= 10;
			if (oldValue < 60 && newValue >= 60)
				m_game.Happiness -= 10;
			if (oldValue < 70 && newValue >= 70)
				m_game.Happiness -= 10;
			if (oldValue < 80 && newValue >= 80)
				m_game.Happiness -= 10;
			if (oldValue < 90 && newValue >= 90){
				if (m_game != null)
					m_game.Happiness -= 10;
			}
		} else { // decreased
			if (oldValue > 40 && newValue <= 40)
				m_game.Happiness += 10;
			if (oldValue > 50 && newValue <= 50)
				m_game.Happiness += 10;
			if (oldValue > 60 && newValue <= 60)
				m_game.Happiness += 10;
			if (oldValue > 70 && newValue <= 70)
				m_game.Happiness += 10;
			if (oldValue > 80 && newValue <= 80)
				m_game.Happiness += 10;
			if (oldValue > 90 && newValue <= 90)
				m_game.Happiness += 10;
		}

	}

	void EnergyChanged (int oldValue, int newValue) {
		
		if (oldValue > 0 && newValue <= 0) {
			m_game.Happiness -= 20;
		}
		else if (oldValue < 0 && newValue > 0) {
			m_game.Happiness += 20;
		}

	}

	void PopulationChanged (int oldValue, int newValue) {
		
		if (oldValue < 200 && newValue >= 200) {
			m_game.m_energyFromPopulation -= 30;
			m_game.Energy -= 30;
			m_game.Waste++;
			m_game.BiosphereDamage++;
			DisplayProgressReport ();
			LOLSDK.Instance.SubmitProgress (0, 4, 16);
			if (m_game.JobsAndAmenities < Config.Me.m_milestoneJobsChecks[0])
				m_game.Happiness -= Config.Me.m_HappinessEffectOfPopulationMilestone;
			else
				m_game.Happiness += Config.Me.m_HappinessEffectOfPopulationMilestone;
		}
		if (oldValue < 300 && newValue >= 300) {
			m_game.m_energyFromPopulation -= 40;
			m_game.Energy -= 40;
			m_game.Waste += 2;
			m_game.BiosphereDamage++;
			DisplayProgressReport ();
			LOLSDK.Instance.SubmitProgress (0, 5, 16);
			Config.Me.m_grid.m_city.m_spriteRenderer.sprite = BuildingData.Me.m_city.m_sprites [1];
			if (m_game.JobsAndAmenities < Config.Me.m_milestoneJobsChecks[1])
				m_game.Happiness -= Config.Me.m_HappinessEffectOfPopulationMilestone;
			else
				m_game.Happiness += Config.Me.m_HappinessEffectOfPopulationMilestone;
		}
		if (oldValue < 400 && newValue >= 400) {
			m_game.m_energyFromPopulation -= 50;
			m_game.Energy -= 50;
			m_game.Waste += 3;
			m_game.BiosphereDamage++;
			DisplayProgressReport ();
			LOLSDK.Instance.SubmitProgress (0, 6, 16);
			if (m_game.JobsAndAmenities < Config.Me.m_milestoneJobsChecks[2])
				m_game.Happiness -= Config.Me.m_HappinessEffectOfPopulationMilestone;
			else
				m_game.Happiness += Config.Me.m_HappinessEffectOfPopulationMilestone;
		}
		if (oldValue < 500 && newValue >= 500) {
			m_game.m_energyFromPopulation -= 60;
			m_game.Energy -= 60;
			m_game.Waste += 3;
			m_game.Pollution++;
			m_game.BiosphereDamage++;
			DisplayProgressReport ();
			LOLSDK.Instance.SubmitProgress (0, 7, 16);
			Config.Me.m_grid.m_city.m_spriteRenderer.sprite = BuildingData.Me.m_city.m_sprites [2];
			if (m_game.JobsAndAmenities < Config.Me.m_milestoneJobsChecks[3])
				m_game.Happiness -= Config.Me.m_HappinessEffectOfPopulationMilestone;
			else
				m_game.Happiness += Config.Me.m_HappinessEffectOfPopulationMilestone;
		}
		if (oldValue < 600 && newValue >= 600) {
			m_game.m_energyFromPopulation -= 70;
			m_game.Energy -= 70;
			m_game.Waste += 4;
			m_game.Pollution++;
			m_game.BiosphereDamage++;
			DisplayProgressReport ();
			LOLSDK.Instance.SubmitProgress (0, 8, 16);
			if (m_game.JobsAndAmenities < Config.Me.m_milestoneJobsChecks[4])
				m_game.Happiness -= Config.Me.m_HappinessEffectOfPopulationMilestone;
			else
				m_game.Happiness += Config.Me.m_HappinessEffectOfPopulationMilestone;
		}
		if (oldValue < 700 && newValue >= 700) {
			LOLSDK.Instance.SubmitProgress (0, 9, 16);
			Debug.Log("GAME WON!");
			Config.Me.m_gui.GameEndedScreen ();
		}

	}

	void DisplayProgressReport () {

		if (m_game == null)
			return;
		if (Config.Me == null)
			return;
		Config.Me.m_gui.m_pause.DisplayMilestone (Config.Me.m_milestones [Mathf.Clamp (m_game.m_milestone, 0, Config.Me.m_milestones.Length - 1)]);
		++m_game.m_milestone;

	}

	void DisplayTutorialBubble(){
	
		if (m_game == null)
			return;
		if (Config.Me == null)
			return;
		if (m_game.m_tutorialCount == 0) {
			ToggleGameTimeRate (false);
			Bubble.Create (Config.Me.m_bubbles [m_game.m_tutorialCount], () => DisplayTutorialBubble());
		}
		if (m_game.m_tutorialCount == 1)
			Bubble.Create (Config.Me.m_bubbles [m_game.m_tutorialCount], () => DisplayTutorialBubble());
		if (m_game.m_tutorialCount == 2)
			Bubble.Create (Config.Me.m_bubbles [m_game.m_tutorialCount], () => DisplayTutorialBubble());
		if (m_game.m_tutorialCount == 3) {
			LOLSDK.Instance.SubmitProgress (0, 2, 16);
			Bubble.Create (Config.Me.m_bubbles [m_game.m_tutorialCount], () => DisplayTutorialBubble (), () => ToggleGameTimeRate (true));
		}
		if (m_game.m_tutorialCount == 4)
			Bubble.Create (Config.Me.m_bubbles [m_game.m_tutorialCount], () => DisplayTutorialBubble());
		if (m_game.m_tutorialCount == 5)
			Bubble.Create (Config.Me.m_bubbles [m_game.m_tutorialCount], () => DisplayTutorialBubble());
		if (m_game.m_tutorialCount == 6)
			Bubble.Create (Config.Me.m_bubbles [m_game.m_tutorialCount], () => DisplayTutorialBubble());
		if (m_game.m_tutorialCount == 7) {
			LOLSDK.Instance.SubmitProgress (0, 3, 16);
			Bubble.Create (Config.Me.m_bubbles [m_game.m_tutorialCount]);
		}
		++m_game.m_tutorialCount;
	
	}

	public void ToggleGameTimeRate (bool isNormal) {
	
		Time.timeScale = isNormal ? 1f : 0f;
		Config.Me.m_gui.m_eHudState = isNormal ? GUIController.E_HudState.none: GUIController.E_HudState.locked;

	}

	void Update () 
	{
		if (m_game != null) {
			if (m_game.IsPhaseOver ()) {
				m_game.EndPhase ();
				Config.Me.m_gui.SetPopulation (m_game.Population);
			}
		}
	}

}
