﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class Grid : MonoBehaviour {

    public class GridSpace
    {
        public Vector3 position;
        public GridBehaviour gridBehaviour;
        public Building building;
        public bool IsOccupied { get { return building != null; } }

        public GridSpace() { }
    }

    public class GridSelection
    {
        public int Width;
        public int Height;
        public Building BuildingData;

        public GridSelection(Building building)
        {
            Width = building.GridWidth;
            Height = building.GridHeight;
            BuildingData = building;
        }
    }

	public CityVisual m_city;
	private static GridSpace m_currentActiveSpace;
	public static GridSpace CurrentActiveSpace {
		get {
			return m_currentActiveSpace;
		}
		set {
			m_currentActiveSpace.gridBehaviour.SetSquareActiveColour (Config.Me.m_inactiveGridColour);
			m_currentActiveSpace = value;
			m_currentActiveSpace.gridBehaviour.SetSquareActiveColour (Config.Me.m_activeGridColour);
		}
	}
    public GridSelection m_selectionData;
	public bool IsPlacingBuilding { get { return m_selectionData != null; } }
    public GridSpace[,] m_grid;
	[SerializeField] private GameObject m_prefabSquare;
    private BuildingVisual m_buildingVisual = null;
    public List<Building> m_constructedBuildings;
	private BuildingVisual m_selectedBuilding;
	public BuildingVisual m_draggedBuilding;
	public BuildingVisual SelectedBuilding {
		get
		{
			return m_selectedBuilding;
		}
		set
		{ 
			if (m_selectedBuilding != null)
				m_selectedBuilding.BuildingDeselected ();
			m_selectedBuilding = value;
		}
	}

	void Start ()
    {
        m_constructedBuildings = new List<Building>();
        SetupGrid();
	}

    public void SetupGrid()
    {
		if (m_grid != null) {
			foreach (var sq in m_grid) {
				sq.gridBehaviour.SetSquareActiveColour (Config.Me.m_inactiveGridColour);
				ObjectPool.Me.PoolObject (sq.gridBehaviour.gameObject);
			}
		}

		m_grid = new GridSpace[Config.Me.m_gridXDimension, Config.Me.m_gridYDimension];
        float xMid = (float)m_grid.GetLength(0) / 2f;
        float zMid = (float)m_grid.GetLength(1) / 2f;

        for (int x = 0; x < m_grid.GetLength(0); ++x)
        {
            for (int z = 0; z < 5; ++z)
            {
                m_grid[x, z] = new GridSpace();
                m_grid[x, z].position = new Vector3((float)x - xMid, 0f, (float)z - zMid);
				var go = ObjectPool.Me.AllocatePrefab (m_prefabSquare.gameObject, true);
				var newSquareInst = go.transform;
                m_grid[x, z].gridBehaviour = newSquareInst.GetComponent<GridBehaviour>();
				m_grid [x, z].gridBehaviour.m_eTerrain = E_TerrainType.SEA;
                newSquareInst.SetParent(this.transform, true);
                newSquareInst.localPosition = m_grid[x, z].position;
			}
			for (int z = 5; z < m_grid.GetLength (1); ++z)
			{
				m_grid[x, z] = new GridSpace();
				m_grid[x, z].position = new Vector3((float)x - xMid, 0f, (float)z - zMid);
				var go = ObjectPool.Me.AllocatePrefab (m_prefabSquare.gameObject, true);
				var newSquareInst = go.transform;
				m_grid[x, z].gridBehaviour = newSquareInst.GetComponent<GridBehaviour>();
				m_grid[x, z].gridBehaviour.m_eTerrain = E_TerrainType.LAND;
				newSquareInst.SetParent(this.transform, true);
				newSquareInst.localPosition = m_grid[x, z].position;
			}
		}

		m_city = CreateCity (BuildingData.Me.m_city) as CityVisual;
		m_city.BuildingPreConstructed();
		var citySpaces = GetCurrentlySelectedSpaces (m_grid [Mathf.RoundToInt (m_grid.GetLength (0) / 2), Mathf.RoundToInt (m_grid.GetLength (1) / 2)]);
		m_city.transform.position = GetCentralPositionForVisual(m_city.transform.position, citySpaces);
		m_city.ToggleCollider(false);
		m_city.PlaceBuilding();
		foreach (var space in citySpaces)
		{
			space.gridBehaviour.SetSquareActiveColour (Config.Me.m_inactiveGridColour);
			space.building = m_city.m_building;
		}
		m_selectionData = null;

		for (int i = 0; i < Config.Me.m_noOfObstacles; ++i) {
			int xCoord = Random.Range (0, m_grid.GetLength (0));
			int yCoord = Random.Range (0, m_grid.GetLength (1));
			var rand = Random.Range (0, BuildingData.Me.Obstacles.Length);
			var obstData = BuildingData.Me.Obstacles [rand];
			m_selectionData = new GridSelection (obstData);
			var spaces = GetCurrentlySelectedSpaces (m_grid [xCoord, yCoord]);
			if (Test_IsValidBuildingSelection (spaces)) {
				foreach (var s in spaces) {
					s.building = obstData;
				}
				var obst = GetNewBuildingInstance (obstData).GetComponent<BuildingVisual> ();
				obst.Spaces = spaces;
				obst.BuildingPreConstructed();
				obst.transform.position = GetCentralPositionForVisual (obst.transform.position, spaces);
				obst.m_building = obstData;
				obst.m_spriteRenderer.sprite = obstData.m_displaySprite;
			}
			else
				--i;
		}

    }

	private GameObject GetNewBuildingInstance (Building building)
	{
		return ObjectPool.Me.AllocatePrefab (BuildingVisual.Create(building).gameObject, false);
	}

    public static GridSpace Test_GetClosestGridSpace(Vector3 pos)
    {
		if (Config.Me.m_grid.m_grid == null)
            return new GridSpace();

        KeyValuePair<GridSpace, float> closest = new KeyValuePair<GridSpace, float> (new GridSpace (), float.PositiveInfinity);
		for (int x = 0; x < Config.Me.m_grid.m_grid.GetLength(0); ++x)
        {
			for (int z = 0; z < Config.Me.m_grid.m_grid.GetLength(1); ++z)
            {
				var sp = Config.Me.m_grid.m_grid[x, z];
                if (sp == null)
                    continue;
                float dist = Vector3.Distance(pos, sp.gridBehaviour.transform.position);
                if (closest.Value > dist)
                    closest = new KeyValuePair<GridSpace, float>(sp, dist);
            }
        }
        return closest.Key;
    }

    public void PlaceNewBuilding(Building building)
    {
        m_selectionData = new GridSelection(building);
		var go = GetNewBuildingInstance (building);
		m_buildingVisual = go.GetComponent<BuildingVisual> ();
        m_buildingVisual.m_building = building;
		m_buildingVisual.m_isPlaced = false;
		m_buildingVisual.m_building = building;
		m_buildingVisual.m_spriteRenderer.sprite = building.m_displaySprite;
		var hitPos = Vector3.zero;
		RaycastHit hit;
		if (Physics.Raycast (Camera.main.ScreenPointToRay (new Vector3 (Screen.width / 2f, Screen.height / 2f, 0f)), out hit, LayerMask.GetMask ("GroundPlane")))
			hitPos = hit.point;
		var space = Test_GetClosestGridSpace (hitPos);
		m_buildingVisual.Spaces = GetCurrentlySelectedSpaces (space);
		m_buildingVisual.transform.position = GetCentralPositionForVisual (m_buildingVisual.transform.position, m_buildingVisual.Spaces);
		Config.Me.m_gui.m_popupScript.CreateConfirmPopupInstance (m_buildingVisual);
	}

	public void RemoveUnplacedBuilding ()
	{
		Config.Me.m_audio.PlayClip (E_AudioClip.AUDIO_DESTROY);
		m_selectionData = null;
		if (m_buildingVisual != null) {
			foreach (var space in m_buildingVisual.Spaces) {
				space.gridBehaviour.SetSquareActiveColour (Config.Me.m_inactiveGridColour);
			}
			if (m_buildingVisual.m_popup != null) {
				Config.Me.m_gui.m_popupScript.RemovePopupInstance (m_buildingVisual.m_popup);
			}
			ObjectPool.Me.PoolObject (m_buildingVisual.gameObject);
		}
	}

	public BuildingVisual CreateCity (Building building) {
	
		m_selectionData = new GridSelection(building);
		var vis = GetNewBuildingInstance (building).GetComponent<BuildingVisual> ();
		var data = vis.m_building as City;
		vis.m_spriteRenderer.sprite = data.m_sprites [0];
		vis.m_building = building;
		m_constructedBuildings.Add(building);
		return vis;
	
	}

    public static List<GridSpace> GetCurrentlySelectedSpaces(GridSpace space)
    {
		if (Config.Me.m_grid == null)
			return null;
		if (Config.Me.m_grid.m_selectionData == null)
            return null;

        List<GridSpace> spaces = new List<GridSpace>();

		int width = Config.Me.m_grid.m_grid.GetLength(0);
		int height = Config.Me.m_grid.m_grid.GetLength(1);
		var index = Config.Me.m_grid.m_grid.FindIndex(space);
        var x = index[0];
        var y = index[1];

		for (int i = 0; i < Config.Me.m_grid.m_selectionData.BuildingData.GridWidth; ++i) {
			for (int ii = 0; ii < Config.Me.m_grid.m_selectionData.BuildingData.GridHeight; ++ii) {
				if (x + i < width && y + ii < height)
					spaces.Add (Config.Me.m_grid.m_grid [x + i, y + ii]);
			}
		}
        return spaces;
    }

    public static bool Test_IsValidBuildingSelection (List<GridSpace> spaces)
    {
		if (Config.Me.m_grid == null || spaces == null)
			return false;
		if (Config.Me.m_grid.m_selectionData == null) {
			return false;
		}
		if (spaces.Count != Config.Me.m_grid.m_selectionData.Height * Config.Me.m_grid.m_selectionData.Width) {
			return false;
		}
        else
        {
            foreach (var space in spaces)
            {
				if (space.IsOccupied || Config.Me.m_grid.m_selectionData.BuildingData.m_eTerrain != space.gridBehaviour.m_eTerrain) {
					return false;
				}
            }
            return true;
        }
    }

    public static Vector3 GetCentralPositionForVisual(Vector3 pos, List<GridSpace> spaces)
    {
        float x = 0;
        float z = 0;
        for (int i = 0; i < spaces.Count; ++i)
        {
            x += spaces[i].gridBehaviour.transform.position.x;
            z += spaces[i].gridBehaviour.transform.position.z;
        }
        x = x / spaces.Count;
        z = z / spaces.Count;

        pos = new Vector3(x, pos.y, z);
        return pos;
    }

    public void PlaceBuilding(Building building, List<GridSpace> spaces)
    {
		Config.Me.m_audio.PlayClip (E_AudioClip.AUDIO_BUILD);
		m_constructedBuildings.Add(building);
		if (Config.Me.m_controller.m_game.m_currentRequiredBuildings.Contains (building))
			Config.Me.m_controller.m_game.m_currentRequiredBuildings.Remove (building);
		m_buildingVisual.ToggleCollider(true);
        m_buildingVisual.PlaceBuilding();
        foreach (var space in spaces)
        {
			space.gridBehaviour.SetSquareActiveColour (Config.Me.m_inactiveGridColour);
            space.building = building;
        }
		building.EnactChanges ();
        m_selectionData = null;
		m_buildingVisual = null;
		m_draggedBuilding = null;
    }

}
