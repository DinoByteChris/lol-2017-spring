﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using LoLSDK;

#if UNITY_EDITOR
using UnityEditor;
#endif

public class QuizGameController : MonoSingleton<QuizGameController> {

	[Serializable]
	public class QuizQuestion
	{
		public string m_question;
		public string m_correctAnswer;
		public string m_incorrectAnswer;
		[TextArea(3, 10)] public string m_reply;
	}

	public class QuizPhase {

		public QuizQuestion m_question;
		public bool m_isFirstCorrect;

		public QuizPhase (QuizQuestion question) {
			m_question = question;
			int rand = UnityEngine.Random.Range (0, 2);
			m_isFirstCorrect = rand == 0;
		}

	}

	[SerializeField] private Animator m_anim;
	[SerializeField] private AudioManager m_audio;
	[SerializeField] private QuizQuestion[] m_questionList;
	[SerializeField] private Text m_question;
	[SerializeField] private Text m_button1Text;
	[SerializeField] private Text m_button2Text;
	[SerializeField] private Text m_reply;
	[SerializeField] private Button m_button1;
	[SerializeField] private Button m_button2;
	[SerializeField] private Button m_next;

	private QuizPhase m_currentPhase;
	private int m_phase;

	protected override void _Awake(){
		m_phase = 0;
		NextQuestion ();
		m_anim.SetTrigger ("Start");
		m_audio.PlayMusic ();
		//LOLSDK.Init ("rgrevwr.wefwef.wefwef");
	}

	public void NextQuestion ()
	{
		if (m_phase >= m_questionList.Length) {
			QuizEnded ();
			return;
		}
		NextQuestion (m_questionList [m_phase]);
		++m_phase;
		ToggleButtonsDisplayed ();

	}

	public void NextQuestion (QuizQuestion question) {
	
		m_currentPhase = new QuizPhase (question);	
		m_question.text = m_currentPhase.m_question.m_question;
		m_button1Text.text = m_currentPhase.m_isFirstCorrect ? m_currentPhase.m_question.m_correctAnswer : m_currentPhase.m_question.m_incorrectAnswer;
		m_button2Text.text = m_currentPhase.m_isFirstCorrect ? m_currentPhase.m_question.m_incorrectAnswer : m_currentPhase.m_question.m_correctAnswer;
		UnityAction action1 = m_currentPhase.m_isFirstCorrect ? new UnityAction (() => CorrectAnswerGiven ()) : new UnityAction (() => IncorrectAnswerGiven ());
		UnityAction action2 = m_currentPhase.m_isFirstCorrect ? new UnityAction (() => IncorrectAnswerGiven ()) : new UnityAction (() => CorrectAnswerGiven ());
		m_button1.onClick.AddListener (action1);
		m_button2.onClick.AddListener (action2);
		m_reply.text = string.Empty;

	}

	void ToggleButtonsDisplayed()
	{
		bool nextButtonIsShown = m_next.gameObject.activeInHierarchy;
		m_next.gameObject.SetActive (!nextButtonIsShown);
		m_button1.gameObject.SetActive (nextButtonIsShown);
		m_button2.gameObject.SetActive (nextButtonIsShown);
	}

	void QuizEnded ()
	{
		LOLSDK.Instance.CompleteGame ();
	}

	public void ExampleFunction () {
		NextQuestion (m_questionList [0]);
	}

	void IncorrectAnswerGiven ()
	{
		m_audio.PlayClip (E_AudioClip.AUDIO_CLICK);
		m_reply.text = "<color=red>That is incorrect. </color>" + m_currentPhase.m_question.m_reply;
	}
	
	void CorrectAnswerGiven ()
	{
		m_audio.PlayClip (E_AudioClip.AUDIO_CLICK);
		m_reply.text = "<color=green>That is correct! </color>" + m_currentPhase.m_question.m_reply;
		LOLSDK.Instance.SubmitProgress (0, 9 + m_phase, 16);
		ToggleButtonsDisplayed ();
	}

	public void Trigger_NextButton ()
	{
		m_audio.PlayClip (E_AudioClip.AUDIO_CLICK);
		m_button1.onClick.RemoveAllListeners ();
		m_button2.onClick.RemoveAllListeners ();
		m_anim.SetTrigger ("Next");
	}

	// --------------------------------------

	#if UNITY_EDITOR
	[CustomEditor(typeof(QuizGameController))]
	public class QuizInspector : Editor
	{
		public override void OnInspectorGUI()
		{
			DrawDefaultInspector();
			var quiz = target as QuizGameController;

			EditorGUILayout.LabelField("Debug Buttons", EditorStyles.boldLabel);

			if (GUILayout.Button("Test"))
				quiz.ExampleFunction();
		}
	}
	#endif

}
