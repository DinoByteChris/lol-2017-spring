﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;

public enum E_TerrainType
{
	LAND,
	BEACH,
	SEA
}

public class GridBehaviour : MonoBehaviour {

    public Renderer m_sprite;
	public E_TerrainType m_eTerrain;
	public Grid.GridSpace m_space;

    public void SetSquareActiveColour(Color colour)
    {
        m_sprite.material.color = colour;
    }

}