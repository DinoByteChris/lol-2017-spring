﻿using UnityEngine;
using UnityEngine.EventSystems;
using System.Collections;
using System.Collections.Generic;

public class BuildingVisual : MonoBehaviour {

	public Animator m_anim;
	[HideInInspector] public Building m_building;
    public Collider m_collider;
	public SpriteRenderer m_spriteRenderer;
	public Popup m_popup;
	public bool m_isPlaced = false;
	private List<Grid.GridSpace> m_spaces;
	[HideInInspector] public List<Grid.GridSpace> Spaces
	{
		get
		{
			return m_spaces;
		}
		set
		{
			if (m_spaces != null){
				foreach (var s in m_spaces) {
					s.gridBehaviour.SetSquareActiveColour (Config.Me.m_inactiveGridColour);
				}
			}
			if (value != null)
			{
				var col = Grid.Test_IsValidBuildingSelection (value) ? Config.Me.m_unoccupiedGridSelectColour : Config.Me.m_occupiedGridSelectColour;
				foreach (var s in value)
				{
					if (!(m_building is Obstacle))
						s.gridBehaviour.SetSquareActiveColour (col);
				}
				transform.position = Grid.GetCentralPositionForVisual (transform.position, value);
			}
			m_spaces = value; 
		}
	}

    public static BuildingVisual Create (Building building)
    {
        BuildingVisual newVisual = null;

		switch (building.m_eGridSize) {
		case E_BuildingSize.Building_1x1:
			newVisual = Config.Me.m_prefab_1x1Building;
			break;
		case E_BuildingSize.Building_2x2:
			newVisual = Config.Me.m_prefab_2x2Building;
			break;
		case E_BuildingSize.Building_3x3:
			newVisual = Config.Me.m_prefab_3x3Building;
			break;
		case E_BuildingSize.Building_3x4:
			newVisual = Config.Me.m_prefab_3x4Building;
			break;
		case E_BuildingSize.Building_3x5:
			newVisual = Config.Me.m_prefab_3x5Building;
			break;
		case E_BuildingSize.Building_4x4:
			newVisual = Config.Me.m_prefab_4x4Building;
			break;
		case E_BuildingSize.Building_4x2:
			newVisual = Config.Me.m_prefab_4x2Building;
			break;
		case E_BuildingSize.Building_5x4:
			newVisual = Config.Me.m_prefab_5x4Building;
			break;
		case E_BuildingSize.Building_5x5:
			newVisual = Config.Me.m_prefab_5x5Building;
			break;
		case E_BuildingSize.Building_4x3:
			newVisual = Config.Me.m_prefab_4x3Building;
			break;
		case E_BuildingSize.City_9x9:
			newVisual = Config.Me.m_prefab_City;
			break;
		default:
			throw new System.ArgumentOutOfRangeException ();
		}
		newVisual.m_building = building;
		newVisual.m_spriteRenderer.sprite = building.m_displaySprite;
        return newVisual;
    }

	public void ConfirmPlacement ()
	{
		if (Grid.Test_IsValidBuildingSelection (Spaces))
		{
			Config.Me.m_grid.PlaceBuilding (m_building, Spaces);
		}
	}

//	public void DestroyStructure () {
//
//		if (Time.timeScale != 0) {
//			if (Config.Me.m_grid.SelectedBuilding != null) {
////				Config.Me.m_grid.SelectedBuilding.RemoveFromGrid ();
//				foreach (var space in Config.Me.m_grid.SelectedBuilding.Spaces) {
//					if (space.building == Config.Me.m_grid.SelectedBuilding.m_building)
//						space.building = null;
//				}
//				ObjectPool.Me.PoolObject (Config.Me.m_grid.SelectedBuilding.gameObject);
//			}
//			else if (Config.Me.m_grid.m_selectionData != null)
//			{
//				Config.Me.m_grid.RemoveUnplacedBuilding ();
//			}
//		}
//	}

//	public void RemoveFromGrid () {
//	
//		foreach (var space in Config.Me.m_grid.m_grid) {
//			if (space.building == m_building)
//				space.building = null;
//		}
//		Destroy (this.gameObject);
//	
//	}
				
	public virtual void BuildingDeselected () {
	
		m_anim.SetBool ("Selected", false);

	}

    public void PlaceBuilding()
    {
		m_isPlaced = true;
        m_anim.SetTrigger("Place");
//		Spaces = null;
    }

    public void ToggleCollider(bool onOrOff)
    {
        m_collider.enabled = onOrOff;
    }

    public void BuildingPreConstructed()
    {
		m_isPlaced = true;
		m_anim.SetTrigger("Constructed");
    }

	void OnDestroy()
	{
		if (Config.Me == null)
			return;
		Config.Me.m_audio.PlayClip (E_AudioClip.AUDIO_DESTROY);
		if (Config.Me.m_grid.SelectedBuilding == this)
			Config.Me.m_grid.SelectedBuilding = null;
		if (m_building != null)
			m_building.ReverseChanges ();
	}

}
