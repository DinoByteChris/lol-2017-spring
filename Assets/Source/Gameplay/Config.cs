﻿using UnityEngine;
using LoLSDK;
using System;
using System.Collections;


#if UNITY_EDITOR
using UnityEditor;
#endif

public class Config : MonoSingleton<Config>
{
	[System.Serializable]
	public class PopulationMilestone
	{
		[TextArea()]
		public string m_text;
		public BuildingEffects[] m_choice1;
		public BuildingEffects[] m_choice2;
	}

    [Header("Scene References")]
    public Transform m_mainCamera;
    public Grid m_grid;
    public GUIController m_gui;
    public GameController m_controller;
	public AudioManager m_audio;
	public Drag m_drag;

    [Header("Prefabs")]
	public BuildingVisual m_prefab_1x1Building;
	public BuildingVisual m_prefab_2x2Building;
	public BuildingVisual m_prefab_3x3Building;
	public BuildingVisual m_prefab_3x4Building;
	public BuildingVisual m_prefab_3x5Building;
	public BuildingVisual m_prefab_4x2Building;
	public BuildingVisual m_prefab_4x3Building;
	public BuildingVisual m_prefab_4x4Building;
	public BuildingVisual m_prefab_5x4Building;
	public BuildingVisual m_prefab_5x5Building;

	public BuildingVisual m_prefab_City;

	[Header("Game Settings")]
	public bool m_exponentialPopulationGrowth;
	public float m_timeToNextPhase = 30.0f;
	public float m_gameTime = 600.0f;
	public float m_startPopulation = 100.0f;
	public float m_endPopulation = 700.0f;
	public int m_gridXDimension = 25;
	public int m_gridYDimension = 25;
	public int m_noOfObstacles = 30;
	public PopulationMilestone[] m_milestones = new PopulationMilestone[5];
	[TextArea()]
	public string[] m_bubbles = new string[8];
	public int m_HappinessEffectOfPopulationMilestone = 5;
	public int[] m_milestoneJobsChecks;

    [Header("GUI Settings")]
    public float m_scrollbarFadeTime = 0.4f;
    public Color m_activeGridColour;
    public Color m_inactiveGridColour;
    public Color m_occupiedGridSelectColour;
    public Color m_unoccupiedGridSelectColour;

	[HideInInspector] public float NoOfPhases { get { return m_gameTime / m_timeToNextPhase; } }
	[HideInInspector] public float PopulationMultiplier { get { return (Mathf.Pow (m_endPopulation / m_startPopulation, 1f / NoOfPhases)); } }

	// --------------------------------------

	protected override void _Awake ()
	{
		LOLSDK.Init ("com.dinobytelabs.greenville");
		m_audio.PlayMusic ();
	}
    
	public void ExampleFunction()
	{
		m_controller.m_game.Waste += 10;
	}

	// --------------------------------------

#if UNITY_EDITOR
    [CustomEditor(typeof(Config))]
    public class QuestContentInspector : Editor
    {
        public override void OnInspectorGUI()
        {
            DrawDefaultInspector();
            var config = target as Config;

            EditorGUILayout.LabelField("Debug Buttons", EditorStyles.boldLabel);

            if (GUILayout.Button("Test"))
                config.ExampleFunction();
        }
    }
#endif

}
